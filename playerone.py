# Pygame template - skeleton for a new pygame project
import pygame
import random
import os
import character
import player
import scenes
import spritesheet
import random
import sys
from enum import Enum

# define things
FPS = 20
WIDTH = 800
HEIGHT = 480

GUBBAR = 3

# define colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
RED_2 = (128, 0, 0)
GREEN = (0, 255, 0)
DARK_GREEN = (0, 128, 0)
BLUE = (0, 0, 255)

class Scene(Enum):
    TITLE = 0
    LVL_1 = 1
    BOSS_DEMO = 10

class GameBasics():
    game_folder = ""
    img_folder = ""
    sound_folder = ""
    font_folder = ""
    characters = None
    sprites = None
    effect = None
    background_image = None
    background_color = None
    text = None
    font = None
    player = None
    

def load_image(path, file):
    unscaled_img = pygame.image.load(os.path.join(path, file)).convert_alpha()
    return pygame.transform.scale(unscaled_img, (100, 100))

def load_animations(sprite_path, sprite_rect, images_cnt, sprite_scale_x, sprite_scale_y):
    images = []

    ss = spritesheet.SpriteSheet(sprite_path)
    all_images = ss.load_strip(sprite_rect, images_cnt, colorkey = -1)

    for unscaled_img in all_images:
        images.append(pygame.transform.scale(unscaled_img, (sprite_scale_x, sprite_scale_y)))

    return images

def start_music(gb, music_path, volume=1.0, rewind=True, loops=-1):
    gb.pygame.mixer.music.load(music_path)
    gb.pygame.mixer.music.set_volume(volume)
    if rewind:
        gb.pygame.mixer.music.rewind()
    gb.pygame.mixer.music.play(loops=loops)

def initTitleScene(gb):
    print("initTitleScene")
    gb.background_color = [BLACK]
    gb.background_image = pygame.image.load(os.path.join(gb.img_folder, "background.png")).convert()

    # Load Sprites
    player_imgs = []
    for a in range(0,12):
        tmp_str = str.format("sprite_elf{:02d}.png".format(a))
        player_imgs.append(load_image(gb.img_folder, tmp_str))  

    # Create characters
    gb.sprites = gb.pygame.sprite.Group()
    gb.characters = []
    seed = 2136930292
    #seed = random.randrange(sys.maxsize)
    random.seed(a=seed)
    print("Seed was:", seed)
    for i in range(0,GUBBAR):
        npc = character.Gubbe(player_imgs, random.randint(0, 7)*110, (random.randint(0, 3)*100))
        gb.characters.append(npc)
        gb.sprites.add(npc)

    # Add Player 1
    images = []
    animations = dict()
    animations['walk'] = load_animations(sprite_path=gb.img_folder+'/knight_walk.png',
                                         sprite_rect=gb.pygame.Rect(0, 1, 14, 14), # x=0, y=1, h=14, w=14
                                         images_cnt=4, 
                                         sprite_scale_x=100,
                                         sprite_scale_y=100)
       
    # Poke
    animations['poke'] = load_animations(sprite_path=gb.img_folder+'/knight_forward_poke.png',
                                         sprite_rect=gb.pygame.Rect(0, 12, 14, 14),
                                         images_cnt=4, 
                                         sprite_scale_x=100,
                                         sprite_scale_y=100)

    gb.player = player.Player(animations, random.randint(0, 7)*110, (random.randint(0, 3)*100))
    gb.characters.append(gb.player)
    gb.sprites.add(gb.player)
        
    # Sound effects
    aow_sound = os.path.join(gb.sound_folder, 'aou_sound.wav')
    gb.effect = gb.pygame.mixer.Sound(aow_sound)
    gb.effect.set_volume(0.2)

    gb.font = gb.pygame.font.Font(os.path.join(gb.font_folder, "wide_pixel-7.ttf"), 36)
    gb.text = gb.font.render("Little Computer People", True, RED_2)

    # start music
    start_music(gb, os.path.join(gb.sound_folder, 'lpc_menu_music.wav'))


def initBossDemo(gb):    
    print("initBossDemo")
    gb.background_color = [DARK_GREEN]
    gb.background_image = None

    # Load Sprites        
    sprite_images = load_animations(sprite_path=gb.img_folder+'/demon_walk.png',
                                         sprite_rect=gb.pygame.Rect(0, 1, 18, 14),
                                         images_cnt=4, 
                                         sprite_scale_x=250,
                                         sprite_scale_y=200)

    # Create characters
    gb.sprites = gb.pygame.sprite.Group()
    gb.characters = []
    boss = character.Gubbe(sprite_images, 110, (100))
    gb.characters.append(boss)
    gb.sprites.add(boss)

    # Add Player 1
    images = []
    gg_images = []
    animations = dict()
    
    # Walk
    animations['walk'] = load_animations(sprite_path=gb.img_folder+'/knight_walk.png',
                                         sprite_rect=gb.pygame.Rect(0, 1, 14, 14),
                                         images_cnt=4, 
                                         sprite_scale_x=100,
                                         sprite_scale_y=100)
    
    # Poke
    
    animations['poke'] = load_animations(sprite_path=gb.img_folder+'/knight_forward_poke.png',
                                         sprite_rect=gb.pygame.Rect(0, 12, 14, 14),
                                         images_cnt=4, 
                                         sprite_scale_x=100,
                                         sprite_scale_y=100)

    gb.player = player.Player(animations, random.randint(0, 7)*110, (random.randint(0, 3)*100))
    gb.characters.append(gb.player)
    gb.sprites.add(gb.player)
        
    # Sound effects
    aow_sound = os.path.join(gb.sound_folder, 'aou_sound.wav')
    gb.effect = gb.pygame.mixer.Sound(aow_sound)
    gb.effect.set_volume(0.2)

    # start music
    start_music(gb, os.path.join(gb.sound_folder, 'lpc_menu_music.wav'))
    
    gb.font = gb.pygame.font.Font(os.path.join(gb.font_folder, "wide_pixel-7.ttf"), 36)
    gb.text = gb.font.render("Little Computer People", True, RED_2)

def scene_change(gb, next_scene, active_scene):
    if next_scene != active_scene:
        print(f"Changing scene {active_scene} -> {next_scene}")
        if next_scene == Scene.TITLE:
            print("Go to Title screen")
            initTitleScene(gb)
            active_scene = next_scene
        if next_scene == Scene.BOSS_DEMO:
            print("Go to Boss demo")
            initBossDemo(gb)
            active_scene = next_scene
    return active_scene

def main():
    print("Hello World!")    

    # initialize pygame and create window
    gb = GameBasics()
    pygame.mixer.init(50000, -16, 1, 1024)
    pygame.init()
    gb.pygame = pygame
    gb.screen = gb.pygame.display.set_mode((WIDTH, HEIGHT))

    # Spelets namn i fönstret
    gb.pygame.display.set_caption("Little Computer People")
    gb.clock = gb.pygame.time.Clock()

    # set up asset folders
    gb.game_folder = os.path.dirname(__file__)
    gb.img_folder = os.path.join(gb.game_folder, 'img')
    gb.sound_folder = os.path.join(gb.game_folder, 'snd')
    gb.font_folder = os.path.join(gb.game_folder, 'font')

    # Game loop
    running = True
    farg = 0
    active_scene = None
    next_scene = Scene.BOSS_DEMO
   

    while running:
        # keep loop running at the right speed
        gb.clock.tick(FPS)

        active_scene = scene_change(gb, next_scene, active_scene)
        
        if active_scene == Scene.TITLE:
            # Process input (events)
            for event in pygame.event.get():
                # check for closing window
                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.MOUSEBUTTONDOWN:

                    for gubbe in gb.characters:
                        if gubbe.rect.collidepoint(event.pos):
                            print("Say Aouw")
                            #gb.effect.play()
                            gubbe.jump()    
                            next_scene = Scene.BOSS_DEMO                        
                    
            keys_pressed = pygame.key.get_pressed()
            if keys_pressed[pygame.K_LEFT]:
                gb.player.userinput(pygame.key.name(pygame.K_LEFT), True)
            if keys_pressed[pygame.K_RIGHT]:
                gb.player.userinput(pygame.key.name(pygame.K_RIGHT), True)
            if keys_pressed[pygame.K_UP]:
                gb.player.userinput(gb.pygame.key.name(gb.pygame.K_UP), True)
            if keys_pressed[pygame.K_DOWN]:
                gb.player.userinput(pygame.key.name(pygame.K_DOWN), True)
            if keys_pressed[pygame.K_SPACE]:
                gb.player.userinput(pygame.key.name(pygame.K_SPACE), True)
            if keys_pressed[pygame.K_LCTRL]:
                gb.player.userinput(pygame.key.name(pygame.K_LCTRL), True)

            # Update sprites
            gb.sprites.update()
            hit = False

            players_shuff = list(gb.characters)
            random.shuffle(players_shuff)
            for gubbe in players_shuff:
                hit = pygame.sprite.spritecollideany(gubbe, gb.sprites)

                if gubbe != hit:
                    print("gubbe hit ", id(gubbe), id(hit), hit)

                    if hit:
                        #gb.effect.play()
                        gubbe.turnaround()
                        hit.turnaround()

            # Draw / render        
            if gb.background_image is not None:
                gb.screen.blit(gb.background_image, [0, 0])

            gb.sprites.draw(gb.screen)
            
            # *after* drawing everything, flip the displayscreen.blit(text,
            text_rect = (400 - gb.text.get_width()/2 + random.randint(0,1), 110 - gb.text.get_height()/2 + random.randint(0,1))
            gb.screen.blit(gb.text, text_rect)

        elif active_scene == Scene.BOSS_DEMO:
            # Process input (events)
            for event in pygame.event.get():
                # check for closing window
                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.MOUSEBUTTONDOWN:

                    for gubbe in gb.characters:
                        if gubbe.rect.collidepoint(event.pos):
                            print("Do something")
                            #gb.effect.play()
                            gubbe.jump()
                            next_scene = Scene.TITLE     
                            
            players_shuff = list(gb.characters)
            random.shuffle(players_shuff)
            for gubbe in players_shuff:
                hit = pygame.sprite.spritecollideany(gubbe, gb.sprites)

                if gubbe != hit:
                    print("gubbe hit ", id(gubbe), id(hit), hit)

                    if hit:
                        #gb.effect.play()
                        gubbe.turnaround()
                        hit.turnaround()
                    
            # Update sprites
            gb.sprites.update()

            # Draw / render        
            if gb.background_image is None:
                gb.screen.fill((0,128,0))#gb.background_color)
            else:
                gb.screen.blit(gb.background_image, [0, 0])
            gb.sprites.draw(gb.screen)
        else:
            active_scene.ProcessInput(pygame.event.get())
            active_scene.Update()
            active_scene.Render()
            
        gb.pygame.display.flip()

    gb.pygame.quit()

if __name__ == "__main__":
    main()
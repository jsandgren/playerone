import pygame
import random
from enum import Enum

class State(Enum):
    STOPPED = 0
    WALK = 1,
    JUMP = 2,
    POKE = 3

class Player(pygame.sprite.Sprite):
    def __init__(self, animations, start_x=0, start_y=0):
        print("Player.init ", id(self))
        pygame.sprite.Sprite.__init__(self)
        self.animations = animations
        self.images =  self.animations['walk']
        self.index = random.randint(0,len(self.images)-1)
        self.image = self.images[self.index]
        self.go_left = 1
        self.direction = "right"

        self.rect = self.image.get_rect()    
        self.rect.x = start_x
        self.rect.y = start_y

        # Set up scaling animation
        self.animate = False
        self.animation_cnt = 0
        self.state = State.WALK
        self.jump_height = 50
        self.jump_cnt = 0
        self.jump_starty = self.rect.y
        (self.screen_width, self.screen_height) = pygame.display.get_surface().get_size()
        self.speed = 0
        self.speed_increased = False
        self.demo_mode = True

    def state_mngmt(self):
        None

    def get_state_images(self, state):
        images = []
        if state == State.WALK:
            images = self.animations['walk']
        elif state == State.POKE:
            images = self.animations['poke']
        else:
            images = self.animations['walk']

        return images

    def update_animation(self):
        if self.state == State.STOPPED:
            self.index = 0
            self.image = self.images[self.index]            
        elif self.state == State.WALK or self.state == State.POKE:
            self.index += 1
            if self.index >= len(self.images):
                self.index = 0
            self.image = self.images[self.index]
        else:
            self.index = 0
            self.image = self.images[self.index]

    def move_sprite(self):
        if self.demo_mode:   
            self.speed_increased = True 
            if self.speed == 0:
                self._increase_speed()
            # flippa the gubbe
            self.rect.x += self.go_left * 5
            if self.rect.right > self.screen_width:
                self.turnaround(left=True)         
            elif self.rect.left <= 0:
                self.turnaround(left=False)

        
        if not self.speed_increased:
            self._decrease_speed()
        self._move_horizontal()
        self.speed_increased = False

    # Körs vid varje ny skärmritning
    def update(self):
        #print("Player.update ", id(self))  
        
        self.state_mngmt()
        self.update_animation()
        self.move_sprite()
            
        if self.animate:
            center_location = self.rect.center
            self.animation_cnt = self.animation_cnt + 1

            # jump
            if self.state == State.JUMP:
                if self.jump_cnt > 5:
                    #going down 
                    self.rect.y = self.jump_starty - self.jump_height + ((float)((self.jump_cnt-5)/5) * self.jump_height)
                else:
                    # going up
                    self.rect.y = self.jump_starty - ((float)(self.jump_cnt/5) * self.jump_height)

                self.jump_cnt = self.jump_cnt + 1
                if self.jump_cnt > 10:
                    self.state = State.WALK
                    self.animate = False
                    self.jump_cnt = 0
                    self.rect.y = self.jump_starty
            elif self.state == State.POKE:
                # start poke animation
                if self.animation_cnt > 4:
                    self.images = self.animations['walk']
                    self.state = State.WALK
                    self.animate = False
                    self.animation_cnt = 0
            else: 
                # walk
                None

    def jump(self):
        if self.state != State.JUMP:
            self.state = State.JUMP
            if self.jump_cnt == 0:
                print("Player.jump ", id(self))
                self.animate = True
                self.jump_cnt = 1
                self.jump_starty = self.rect.y

    def poke(self):
        print("Player.poke ", id(self))
        if self.state != State.POKE:
            self.state = State.POKE
            self.animate = True
            self.animation_cnt = 1
            self.images = self.animations['poke']

    def _flip_imgs(self):
        tmp_list = self.images
        self.images = []
        self.index = 0

        # flippa alla gubbens bilder
        for img in tmp_list:
            self.images.append(pygame.transform.flip(img, True, False))

    def _move_horizontal(self):   
        if not self.state == State.POKE:     
            self.rect.x += self.go_left * self.speed
            if self.rect.right > self.screen_width:
                self.turnaround(left=True)         
            elif self.rect.left <= 0:
                self.turnaround(left=False)

    def _increase_speed(self):
        self.speed_increased = True
        if self.speed < 5:
            self.speed = self.speed + 1

    def _decrease_speed(self):
        if self.speed > 0:
            self.speed = self.speed - 1
            
    def _reset_speed(self):
        self.speed = 0

    def userinput(self, key, press):
        print("Player.userinput ", self.direction, key, press)
        if press:
            if key == "left":
                if self.direction == "right":
                    self.direction == "left"
                    self.turnaround(left=True)
                    self._reset_speed()
                else:
                    self._increase_speed()
                    
            elif key == "right":
                if self.direction == "left":
                    self.direction == "right"
                    self.turnaround(left=False)
                    self._reset_speed()
                else:
                    self._increase_speed()

            if key == "up":
                self.rect.y = self.rect.y - 5
            elif key == "down":
                self.rect.y = self.rect.y + 5

            if key == "space":
                self.jump()

            if key == "left ctrl":
                self.poke()

    def turnaround(self, left=None):
        if left == None:
            self.go_left = self.go_left * -1
            self._flip_imgs()
        elif left is True:
            if self.go_left == 1:
                self._flip_imgs()
            self.go_left = -1
        else:
            if self.go_left == -1:
                self._flip_imgs()
            self.go_left = 1
            
        self.rect.x += self.go_left * 5

        if self.go_left == -1:
            self.direction = "left"
        else:            
            self.direction = "right"
   
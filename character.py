import pygame
import random

class Gubbe(pygame.sprite.Sprite):
    def __init__(self, images, start_x=0, start_y=0):
        print("Gubbe.init ", id(self))
        pygame.sprite.Sprite.__init__(self)
        self.images = images
        self.index = random.randint(0,len(images)-1)
        self.image = self.images[self.index]
        self.go_left = 1

        self.rect = self.image.get_rect()    
        self.rect.x = start_x
        self.rect.y = start_y

        # Set up scaling animation
        self.animate = False
        self.jump_height = 50
        self.jump_cnt = 0
        self.jump_starty = self.rect.y
        (self.screen_width, self.screen_height) = pygame.display.get_surface().get_size()


    # Körs vid varje ny skärmritning
    def update(self):        

        # change sprite        
        self.index += 1
        if self.index >= len(self.images):
            self.index = 0
        self.image = self.images[self.index]

        # flippa the gubbe
        self.rect.x += self.go_left * 7
        if self.rect.right > self.screen_width:
            self.turnaround(left=True)         
        elif self.rect.left <= 0:
            self.turnaround(left=False)

            
        if self.animate:
            center_location = self.rect.center

            # jump
            if self.jump_cnt > 5:
                #going down 
                self.rect.y = self.jump_starty - self.jump_height + ((float)((self.jump_cnt-5)/5) * self.jump_height)
            else:
                # going up
                self.rect.y = self.jump_starty - ((float)(self.jump_cnt/5) * self.jump_height)

            self.jump_cnt = self.jump_cnt + 1
            if self.jump_cnt > 10:
                self.animate = False
                self.jump_cnt = 0
                self.rect.y = self.jump_starty

    #def blinking(self):
        

    def jump(self):
        if self.jump_cnt == 0:
            print("Gubbe.jump ", id(self))
            self.animate = True
            self.jump_cnt = 1
            self.jump_starty = self.rect.y

    def _flip_imgs(self):
        tmp_list = self.images
        self.images = []
        self.index = 0

        # flippa alla gubbens bilder
        for img in tmp_list:
            self.images.append(pygame.transform.flip(img, True, False))    

    def turnaround(self, left=None):
        print("Gubbe.turnaround ",id(self))

        if left == None:
            self.go_left = self.go_left * -1
            self._flip_imgs()
        elif left is True:
            if self.go_left == 1:
                self._flip_imgs()
            self.go_left = -1
        else:
            if self.go_left == -1:
                self._flip_imgs()
            self.go_left = 1
            
        self.rect.x += self.go_left * 5
   